import * as config from './config'
import { Server } from './server'
import { MailService } from './services/mailer'
import { SentryPlugin } from './plugins/sentry'
import { LoggerPlugin } from './plugins/logger'
import { MaildevPlugin } from './plugins/maildev'
import { SwaggerPlugin } from './plugins/swagger.ts'

;(async function main () {
  const serverConf = config.getServerConfiguration()
  const mailConfig = config.getMailConfig()
  // To be used in forms
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const mailService = new MailService(mailConfig)

  const server = new Server(serverConf, [
    new LoggerPlugin(),
    new SwaggerPlugin(
      config.getSwaggerConfiguration(),
      config.getServerConfiguration()
    ),
    new SentryPlugin(config.getSentryConfiguration()),
    new MaildevPlugin(mailConfig, config.getServerConfiguration())
  ])

  await server.start()

  console.log(`Server running at: ${server.info.uri}`)
})().catch((err) => {
  console.error(`Error starting the application: ${err}`)
  process.exit(1)
})
