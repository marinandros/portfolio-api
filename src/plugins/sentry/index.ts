import * as Hapi from '@hapi/hapi'
import { IPlugin, IPluginInfo } from '../../interfaces'
import { ISentryConfiguration } from '../../config'

export class SentryPlugin implements IPlugin {
  private readonly name = 'sentry'
  private readonly version = '1.0.0'

  constructor (
    private readonly sentryConfig: ISentryConfiguration
  ) {}

  // eslint-disable-next-line @typescript-eslint/ban-types
  async register (server: Hapi.Server, options?: object): Promise<void> {
    if (this.sentryConfig.dsn) {
      await server.register({
        plugin: require('hapi-sentry'),
        options: {
          client: {
            dsn: this.sentryConfig.dsn,
            release: process.env.RELEASE,
            environment: process.env.NODE_ENV
          }
        }
      })

      server.ext({
        type: 'onPostAuth',
        method (request, h) {
          if (request.payload) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (request as any).sentryScope.setExtra('payload', request.payload)
          }
          return h.continue
        }
      })
    }
  }

  info (): IPluginInfo {
    const { name, version } = this
    return { name, version }
  }
}
