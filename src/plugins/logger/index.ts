import * as Hapi from '@hapi/hapi'
import { IPlugin, IPluginInfo } from '../../interfaces'

export class LoggerPlugin implements IPlugin {
  private readonly name = 'logger'
  private readonly version = '1.0.0'

  async register (server: Hapi.Server): Promise<void> {
    if (process.env.NODE_ENV !== 'test') {
      return server.register({
        plugin: require('@hapi/good'),
        options: {
          ops: {
            interval: 1000
          },
          reporters: {
            consoleReporter: [
              {
                module: '@hapi/good-squeeze',
                name: 'Squeeze',
                args: [
                  {
                    log: '*',
                    error: '*',
                    request: '*',
                    response: '*'
                  }
                ]
              },
              {
                module: '@hapi/good-console'
              },
              'stdout'
            ]
          }
        }
      })
    }
  }

  info (): IPluginInfo {
    const { name, version } = this
    return { name, version }
  }
}
