import * as Hapi from '@hapi/hapi'
import * as H2o2 from '@hapi/h2o2'
import * as MailDev from 'maildev'
import { promisify } from 'util'
import { IPlugin, IPluginInfo } from '../../interfaces'
import { IMailConfiguration, IServerConfiguration } from '../../config'

export class MaildevPlugin implements IPlugin {
  private readonly name = 'maildev'
  private readonly version = '1.0.0'

  private readonly webPort = 1080
  private readonly routePath = '/maildev'
  private readonly basePath: string

  constructor (
    private readonly mailConfig: IMailConfiguration,
    serverConfig: IServerConfiguration
  ) {
    this.basePath = (serverConfig.routePrefix || '') + this.routePath
  }

  async register (server: Hapi.Server): Promise<void> {
    if (process.env.NODE_ENV === undefined || process.env.NODE_ENV === 'development') {
      const { webPort, basePath, mailConfig } = this
      const maildev = new MailDev({
        ip: mailConfig.host,
        smtp: mailConfig.port,
        incomingUser: mailConfig.username,
        incomingPass: mailConfig.password,
        web: webPort,
        basePathname: basePath,
        silent: true
      })

      await promisify(maildev.listen.bind(maildev))()

      await this.registerRoutes(server)
    }
  }

  private async registerRoutes (server: Hapi.Server): Promise<void> {
    const { routePath, webPort } = this

    await server.register(H2o2)

    server.route({
      method: '*',
      path: `${routePath}/{p*}`,
      options: {
        auth: false
      },
      handler: {
        proxy: {
          host: 'localhost',
          port: webPort,
          xforward: true,
          passThrough: true
        }
      }
    })
  }

  info (): IPluginInfo {
    const { name, version } = this
    return { name, version }
  }
}
