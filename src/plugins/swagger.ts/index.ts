import * as path from 'path'
import * as Hapi from '@hapi/hapi'
import * as Inert from '@hapi/inert'
import * as config from '../../config'
import * as Vision from '@hapi/vision'
import * as HapiSwagger from 'hapi-swagger'
import { IPlugin, IPluginInfo } from '../../interfaces'

export class SwaggerPlugin implements IPlugin {
  private readonly name = 'swagger'
  private readonly version = '1.0.0'

  private readonly docsPath = '/docs'
  private readonly basePath: string
  private readonly pathPrefixSize: number

  constructor (
    private readonly swaggerConfig: config.ISwaggerConfiguration,
    private readonly serverConfig: config.IServerConfiguration
  ) {
    this.basePath = (this.serverConfig.routePrefix || '') + '/'
    this.pathPrefixSize = this.basePath.split('/').length - 1
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async register (server: Hapi.Server, options?: object): Promise<void> {
    const { swaggerConfig: config, basePath, pathPrefixSize, docsPath } = this
    const host = config.host || `${server.info.host}:${server.info.port}`
    const schemaArr = typeof config.schemas === 'string'
      ? config.schemas.split(',').map((item) => item.trim())
      : config.schemas
    const schemas = schemaArr.length ? schemaArr : [server.info.protocol]
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const pkg = require(path.join(process.cwd(), 'package.json'))

    const swaggerOptions: HapiSwagger.RegisterOptions = {
      info: {
        title: 'Portfolio API',
        description: 'Api service for Marin Andros Portfolio page',
        version: pkg.version
      },
      securityDefinitions: {
        jwt: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
        }
      },
      security: [{ jwt: [] }],
      host: host,
      schemes: schemas,
      basePath: basePath,
      pathPrefixSize: pathPrefixSize,
      swaggerUI: true,
      documentationPage: true,
      documentationPath: docsPath
    }

    return server.register([
      {
        plugin: Inert
      },
      {
        plugin: Vision
      },
      {
        plugin: HapiSwagger,
        options: swaggerOptions
      }
    ])
  }

  info (): IPluginInfo {
    const { name, version } = this
    return { name, version }
  }
}
