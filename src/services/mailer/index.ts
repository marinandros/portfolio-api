import * as Nodemailer from 'nodemailer'
import { IMailConfiguration } from '../../config'
import { IMailService, IMailSendOptions } from './interfaces'

export * from './interfaces'

export class MailService implements IMailService {
  private readonly transport: Nodemailer.Transporter

  constructor (config: IMailConfiguration) {
    this.transport = Nodemailer.createTransport({
      host: config.host,
      port: config.port,
      secure: (+config.port === 465),
      auth: {
        user: config.username,
        pass: config.password
      },
      tls: {
        rejectUnauthorized: false
      }
    })
  }

  async send (options: IMailSendOptions): Promise<void> {
    return this.transport.sendMail(options)
  }
}
