export interface IMailSendOptions {
  to: string
  from: string
  subject: string
  text?: string
  html?: string
}

export interface IMailService {
  send (opts: IMailSendOptions): Promise<void>
}
