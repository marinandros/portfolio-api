import * as Hapi from '@hapi/hapi'

export interface IPluginInfo {
  name: string
  version: string
}

export interface IPlugin {
  info (): IPluginInfo
  register (server: Hapi.Server, opts?: Record<string, unknown>): Promise<void>
}
