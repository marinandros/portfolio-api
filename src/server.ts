import * as Hapi from '@hapi/hapi'
import { IPlugin } from './interfaces'
import { IServerConfiguration } from './config'

export class Server {
  readonly hapi: Hapi.Server
  private initialized = false

  constructor (
    private readonly configuration: IServerConfiguration,
    private readonly plugins: IPlugin[]
  ) {
    const options: Hapi.ServerOptions = {
      port: configuration.port,
      routes: {
        cors: {
          origin: ['*'],
          credentials: true,
          additionalHeaders: ['X-Requested-With']
        },
        validate: {
          failAction (request: Hapi.Request, h: Hapi.ResponseToolkit, err?: Error): Error {
            throw err
          }
        },
        response: {
          modify: true,
          options: {
            stripUnknown: true
          }
        }
      }
    }
    
    if (process.env.NODE_ENV === 'development') {
      options.debug = {
        request: ['error']
      }
    }

    this.hapi = new Hapi.Server(options)
    this.hapi.route({
      method: 'GET',
      path: this.configuration.routePrefix ? `${this.configuration.routePrefix}/` : '/',
      options: {
        auth: false,
        description: 'Health check route',
        tags: ['api']
      },
      handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
        return { status: 'success' }
      }
    })
  }

  async init () {
    if (this.initialized) return
    this.initialized = true 

    const { hapi, plugins, configuration } = this
    const registerOptions: Hapi.ServerRegisterOptions = {}
    if (configuration.routePrefix) {
      registerOptions.routes = { prefix: configuration.routePrefix }
    }

    for (const plug of plugins) {
      const info = plug.info()
      const register = plug.register.bind(plug)
      const plugin = { register, ...info }
      await hapi.register(plugin, registerOptions)
    }
  }

  async start () {
    await this.init()
    this.hapi.start()
  }

  async stop (options?: { timeout: number }) {
    return this.hapi.stop(options)
  }

  get info (): Hapi.ServerInfo {
    return this.hapi.info
  }
}