import * as config from 'config'

export interface IServerConfiguration {
  port?: string
  routePrefix?: string
}

export interface ISwaggerConfiguration {
  host: string
  schemas: string | string[]
}

export interface ISentryConfiguration {
  dsn: string
}

export interface IMailConfiguration {
  host: string
  port: number
  username: string
  password: string
  from: string
}

export function getServerConfiguration (): IServerConfiguration {
  return config.get('server') || {}
}

export function getSwaggerConfiguration (): ISwaggerConfiguration {
  return config.get('swagger') || {}
}

export function getSentryConfiguration (): ISentryConfiguration {
  return config.get('sentry') || {}
}

export function getMailConfig (): IMailConfiguration {
  return config.get('mail') || {}
}
